CC=/opt/gbdk/bin/lcc

burgeria.gb: game.c
	$(CC) -o burgeria.gb game.c
test.gb: test.c
	$(CC) -o test.gb test.c
clean:
	rm -f  *.gb *.map *.sym
