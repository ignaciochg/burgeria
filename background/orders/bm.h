/*

 BM.H

 Map Include File.

 Info:
   Section       : 
   Bank          : 0
   Map size      : 2 x 1
   Tile set      : Z:\home\nacho\CPE481\game\background\bkTiles.gbr
   Plane count   : 0.5 plane (4 bits)
   Plane order   : Tiles are continues
   Tile offset   : 0
   Split data    : No

 This file was generated by GBMB v1.8

*/

#define burgerMidWidth 2
#define burgerMidHeight 1
#define burgerMidBank 0

extern unsigned char burgerMid[];

/* End of BM.H */
