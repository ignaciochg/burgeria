/*

 0.C

 Map Source File.

 Info:
   Section       : 
   Bank          : 0
   Map size      : 1 x 2
   Tile set      : Z:\home\nacho\CPE481\game\background\bkTiles.gbr
   Plane count   : 1 plane (8 bits)
   Plane order   : Tiles are continues
   Tile offset   : 0
   Split data    : No

 This file was generated by GBMB v1.8

*/

#define digit0Width 1
#define digit0Height 2
#define digit0Bank 0

unsigned char digit0[] =
{
  0x16,0x17
};

/* End of 0.C */
