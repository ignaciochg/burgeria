/*

notes:

preload the tiles for each screen before displaying them in the main loop 
whenever we place a patty draw it , if we trash it overwrite the tile with blank tile

*/


#include <gb/gb.h>
#include <gb/drawing.h>
#include <gb/console.h>
#include <time.h>
#include <stdio.h>
#include <rand.h>
fixed seed;

#include "sprites/numbers.c"
#include "sprites/actions.c"
#include "sprites/blank.c"
#include "sprites/bottomBurgers.c"
#include "sprites/topBurgers.c"
#include "background/STORE.C"
#include "background/STORE.M"
#include "background/grillMap.c"
#include "background/assemblyMap.c"
#include "background/grillAssemblyTiles.c"
#include "background/numbers/numbers.c"
#include "background/burgers/burgers.c"
#include "background/actions/actions.c"
#include "background/orders/ticketItems.c"

#include "sprites/hand.c"

#define TRUE       1
#define FALSE      0
#define LAST_LEVEL 1
#define OFFSET     8
#define MAX_INT    255

#define OUTSIDE  0
#define STORE    1
#define GRILL    2
#define ASSEMBLY 3

#define NUMB_PATTIES 4
#define MAX_PATTY_STORAGE 10
#define NUMB_GRILL_ASSEMBLY_TILES 97

#define BURGER0_X 70
#define BURGER0_Y 70
#define BURGER1_X 102
#define BURGER1_Y 70
#define BURGER2_X 70
#define BURGER2_Y 86
#define BURGER3_X 102
#define BURGER3_Y 86
#define NEW_PATTY_X 40
#define NEW_PATTY_Y 20
#define FLIP_X 48
#define FLIP_Y 20
#define DONE_X 56
#define DONE_Y 20
#define TRASH_X 64
#define TRASH_Y 20
 
//assembly  sprite definitions


//infredients ID
#define NUMB_FOOD_ITEMS 9
#define TOP_BUN 0
#define BOT_BUN 1
#define BURGER_R  2
#define BURGER_M  3
#define BURGER_D  4
#define BURGER_B  5
#define TOMATO  6
#define CHEESE  7
#define LETUCE  8
//tile not implemented, so not implemented in game
#define BACON   9
#define KETCHUP 10
#define MAYO    11
#define MUSTARD 12

//define grill actions ids
#define NUMB_ACTIONS_GRILL 4
#define PLACE_PATTY 0
#define FLIP_PATTY  1
#define READY_PATTY 2
#define TRASH_PATTY 3

#define NUMB_ACTIONS_ASSEMBLY 4
#define PICK_ITEM 0
#define PICK_PATTY  1
#define READY_TICKET 2
#define TRASH_PATTY 3


#define ARM_TILE 0
#define OPEN_HAND_TILE 4
#define CLOSED_HAND_TILE 8
#define FIST_TILE 12

// //define tile names, 0-9 are used by numbers
// #define BURGER_TOP0_TILE 10
// #define BURGER_TOP1_TILE 11
// #define BURGER_TOP2_TILE 12
// #define BURGER_TOP3_TILE 13
// #define BURGER_BOT0_TILE 14
// #define BURGER_BOT1_TILE 15
// #define BURGER_BOT2_TILE 16
// #define BURGER_BOT3_TILE 17
// #define PATTY_TILE       18
// #define FLIP_TILE        19
// #define READY_TILE       20
// #define TRASH_TILE       21
// #define ACTION_HL0_TILE  22
// #define ACTION_HL1_TILE  23
 #define BLANK_TILE       24

typedef UINT8 bool;

bool A_PRESSED      = FALSE;
bool B_PRESSED      = FALSE;
bool UP_PRESSED     = FALSE;
bool DOWN_PRESSED   = FALSE;
bool LEFT_PRESSED   = FALSE;
bool RIGHT_PRESSED  = FALSE;
bool SELECT_PRESSED = FALSE;
bool START_PRESSED  = FALSE;

UINT8 temp = 0;
UINT8 currentLevel = 0;
UINT8 currentLocation = OUTSIDE; 
time_t currentTime = 0;
time_t lastTime = clock();
time_t timeSum = 0;
bool refresh = FALSE;
bool paused = FALSE; 
UINT8 nextOrderID = 0;
UINT8 handX = 10;
UINT8 handY = 38;
UINT8 x = 0;
UINT8 y = 0;
bool handOpen = FALSE;
UINT16 index = 0;
UINT8 grillCursor = 0;
UINT8 actionCursor = 0;
bool burgers[NUMB_PATTIES] = {FALSE,FALSE,FALSE,FALSE}; // all 4 burgers in grill are not placed
UINT8 burgers_top_cook[NUMB_PATTIES] = {0,0,0,0};
UINT8 burgers_bot_cook[NUMB_PATTIES] = {0,0,0,0};
UINT8 pattyStorage[MAX_PATTY_STORAGE*2];// will be stored in a top bottom configuration
UINT8 pattyStorageCount = 0;
UINT8 pattyStogareIndex = 0;
// bool hl2 = FALSE;
UINT8 foodItemCursor = 0;

UINT8 timeHours = 12;
UINT8 timeMinutes = 0;



struct Ticket{
   UINT8 orderID;
   UINT8 ingredients[10];//ingredient 0 should be the bottom bun, len-1 should be top bun
   UINT8 ingredients_size;
   UINT8 patty_tops[3];
   UINT8 patty_bots[3];
   UINT8 number_patties;
};
void initilizeTicket(struct Ticket *tik){ // to call use initializeTicket(&objectName)
   tik->ingredients_size = 0;
   tik->number_patties = 0;
}


#define MAX_PENDING_ORDERS 5
struct Ticket pendingOrders[MAX_PENDING_ORDERS];
UINT8 pendingOrderSize = 0;
UINT8 selectedOrder = 0;

void runEveryLoop();
bool loadExists();
void loadSaved();
void updaterTimer(time_t inTicks);//input is how many ticks to trigger
void resetTimerFlag();
UINT8 getNewOID();
void displayNumber(UINT16 numb, UINT8 X, UINT8 Y, UINT8 sprite0, UINT8 sprite1, UINT8 sprite2);
void resetPressedKeys();
void updateInput();

void placePatty(UINT8 pattyNumb);
void flipPatty(UINT8 pattyNumb);
void readyPatty(UINT8 pattyNumb);
void trashPatty(UINT8 pattyNumb);
void pew();

void setOderNumber(UINT8 numb);
void placeDigit(UINT8 digit,UINT8 x, UINT8 y);
void setTime(UINT8 hours,UINT8 minutes);
void displayPatty(UINT8 patty,UINT8 top,UINT8 bot);
void displayActions(UINT8 action,UINT8 screen);

void displayHand(UINT8 spriteNumb);
void moveHand(UINT8 spriteNumb, UINT8 x,UINT8 y);

void displayTicket(struct Ticket* tik, UINT8 location);//pass int addres of object using the & operator

void randomTicketGenerator(struct Ticket* tik); //pass in the address of exixting ticket
UINT8 burgerClass(UINT8 cookness);//0:Raw,1:Medium,2:Done,3:Burnt
void newPendingOrder();
void ClearAllTiles();
void displayFoodItem(UINT8 itemID);
void displayGrabable();

 
void init(){
   
   seed.b.l = DIV_REG;//generate park of a random seed for rand

   //load background 

   if(loadExists()){ //if save exists load ask used if he wants to load
      //FIX press A to load save otherwise start from 0
      loadSaved();
   }
   pew();

   SPRITES_8x8;
   set_sprite_data(0,16,hand);
   //set_sprite_data(0, 10, numbers);
   //set_sprite_data(BURGER_TOP0_TILE, 4, burgerTops);
   //set_sprite_data(BURGER_BOT0_TILE, 4, burgerBots);
   //set_sprite_data(PATTY_TILE, 6, actionSet);
   set_sprite_data(BLANK_TILE, 1, blank);

   SHOW_SPRITES;
   SHOW_BKG;

  seed.b.h = DIV_REG;//generate the rest of the random seed for rand
  //initilizeTicket(&(pendingOrders[0]));
  //initilizeTicket(&(pendingOrders[1]));
  //initilizeTicket(&(pendingOrders[2]));
  //initilizeTicket(&(pendingOrders[3]));
  //initilizeTicket(&(pendingOrders[4]));
   newPendingOrder();
   newPendingOrder();
   newPendingOrder();
   newPendingOrder();

}


void main(){
   init();

   while(1){
      runEveryLoop();

      if(paused){
         //printf("pause menu\n");
         if(B_PRESSED){
            B_PRESSED = FALSE;
            paused = FALSE;
            delay(100);
         }
      }
      else{//main game

         if(currentLevel == 1){//will generate orders and ustomers of level 1

         }
         else if(currentLevel > LAST_LEVEL){// if we beat all leavels execute this
            //printf("GAME FINISHED\n");
         }

         switch(currentLocation){
            case OUTSIDE:
               while(index <= 100){
                  printf("outside %d/100\n",index);
                  index++;
               }
               currentLocation++;
               set_bkg_data(0, 104 , storeTiles);
               VBK_REG = 1;
            set_bkg_tiles(0,0,20,18,storeMap);
               VBK_REG = 0; 
            SHOW_BKG;
               break;
            case STORE:
               //printf("store\n");
               break;
            case GRILL:
            //printf("%d%d%d%d%d%d%d%d%d\n",); 
            //printf("%d,%d,%d,%d,%d,%d\n",actionCursor,grillCursor,burgers[0],burgers[1],burgers[2],burgers[3]);
            //printf("%d,%d\n",actionCursor,grillCursor);

               /*index = 0;
               while(index < NUMB_PATTIES){
                  set_sprite_tile(index, BURGER_TOP0_TILE);
                  move_sprite(sprite0,X,Y);
                  displayNumber(456,70,70,0,1,2);
                  displayNumber(457,70,78,0,1,2);
               } 
               displayNumber(456,70,70,0,1,2);
               displayNumber(457,70,78,0,1,2);
*/
               //displayNumber(456,70,78,0);
               //displayNumber(456,70,86,0);
               
               for(index = 0; index < NUMB_PATTIES; index++){
                  if(burgers[index]){/*
                     switch(index){
                        case 0:
                           x =BURGER0_X;
                           y =BURGER0_Y;
                           break;
                        case 1:
                           x =BURGER1_X;
                           y =BURGER1_Y;
                           break;
                        case 2:
                           x =BURGER2_X;
                           y =BURGER2_Y;
                           break;
                        case 3:
                           x =BURGER3_X;
                           y =BURGER3_Y;
                           break;
                     }
                     move_sprite((index*5),x,y);
                     move_sprite((index*5)+1,x,y);*/
                     // UINT8 bot = 0;
                     // UINT8 top = 0;
                     // if(burgers_top_cook[index] < 70){//if top is raw
                     //    //set_sprite_tile((index*5), BURGER_TOP0_TILE);
                     //    top = 0;
                     // }
                     // else if(burgers_top_cook[index] >= 70 && burgers_top_cook[index] < 90){//if top is medium
                     //    //set_sprite_tile((index*5), BURGER_TOP1_TILE);
                     //    top = 1;
                     // }
                     // else if(burgers_top_cook[index] >= 90){ //if top is done
                     //    //set_sprite_tile((index*5), BURGER_TOP2_TILE);
                     //    top = 2;
                     //    burgers_top_cook[index] = 91;
                     // }
                     // //else if(burgers_top_cook[index] >= 110){
                     // //   set_sprite_tile((index*5), BURGER_TOP3_TILE);
                     // //}
                     // if(burgers_bot_cook[index] < 70){//if bot is raw
                     //    //set_sprite_tile((index*5)+1, BURGER_BOT0_TILE);
                     //    bot = 0;
                     // }
                     // else if(burgers_bot_cook[index] >= 70 && burgers_bot_cook[index] < 90){//if bot is done
                     //    //set_sprite_tile((index*5)+1, BURGER_BOT1_TILE);
                     //    bot = 1;
                     // }
                     // else if(burgers_bot_cook[index] >= 90){//if bot is done
                     //    //set_sprite_tile((index*5)+1, BURGER_BOT2_TILE);
                     //    bot = 2;
                     //    burgers_bot_cook[index] = 91;
                     // }
                     // //else if(burgers_bot_cook[index] >= 110){
                     // //   set_sprite_tile((index*5)+1, BURGER_BOT3_TILE);
                     // //}

                     if (burgers_top_cook[index] > 245){
                        burgers_top_cook[index] = 245;
                     }
                     if (burgers_bot_cook[index] > 245){
                        burgers_bot_cook[index] = 245;
                     }
                     //displayNumber(burgers_bot_cook[index],x,y+OFFSET,(index*5)+2,(index*5)+3,(index*5)+4);
                     displayPatty(index,burgerClass(burgers_top_cook[index]),burgerClass(burgers_bot_cook[index]));//display empty patty

                  }
                  else{// move to 00, and make blank
                     displayPatty(index,4,4);//display empty patty
                     /*set_sprite_tile((index*5), BLANK_TILE);
                     set_sprite_tile((index*5)+1, BLANK_TILE);
                     set_sprite_tile((index*5)+2, BLANK_TILE);
                     set_sprite_tile((index*5)+3, BLANK_TILE);
                     set_sprite_tile((index*5)+4, BLANK_TILE);
                     move_sprite((index*5), 0, 0);
                     move_sprite((index*5)+1, 0, 0);
                     move_sprite((index*5)+2, 0, 0);
                     move_sprite((index*5)+3, 0, 0);
                     move_sprite((index*5)+4, 0, 0);*/
                  }
               }
               /*index = index*5;//fix index, to be used by next sprite
               set_sprite_tile(index, PATTY_TILE);
               move_sprite(index++, NEW_PATTY_X, NEW_PATTY_Y);
               set_sprite_tile(index, FLIP_TILE);
               move_sprite(index++, FLIP_X, FLIP_Y);
               set_sprite_tile(index, READY_TILE);
               move_sprite(index++, DONE_X, DONE_Y);
               set_sprite_tile(index, TRASH_TILE);
               move_sprite(index++, TRASH_X, TRASH_Y);
               if(hl2){//will choose one or the other highlight
                  set_sprite_tile(index, ACTION_HL0_TILE);
               }
               else{
                  set_sprite_tile(index, ACTION_HL1_TILE);
               }
               switch(actionCursor){
                  case PLACE_PATTY:
                     move_sprite(index++, NEW_PATTY_X, NEW_PATTY_Y);
                     break;
                  case FLIP_PATTY:
                     move_sprite(index++, FLIP_X, FLIP_Y);
                     break;
                  case READY_PATTY:
                     move_sprite(index++, DONE_X, DONE_Y );
                     break;
                  case TRASH_PATTY:
                     move_sprite(index++, TRASH_X, TRASH_Y);
                     break;
               } */
               

               if(LEFT_PRESSED){
                  LEFT_PRESSED = FALSE;
                  switch(grillCursor){
                     case 1:
                        grillCursor = 0;
                        break;
                     case 3:
                        grillCursor = 2;
                        break;
                     default:
                        break;
                  }
               }
               if(RIGHT_PRESSED){
                  RIGHT_PRESSED = FALSE;
                  switch(grillCursor){
                     case 0:
                        grillCursor = 1;
                        break;
                     case 2:
                        grillCursor = 3;
                        break;
                     default:
                        break;
                  }
               } 
               if(UP_PRESSED){
                  UP_PRESSED = FALSE;
                  switch(grillCursor){
                     case 2:
                        grillCursor = 0;
                        break;
                     case 3:
                        grillCursor = 1;
                        break;
                     default:
                        break;
                  }
               } 
               if(DOWN_PRESSED){
                  DOWN_PRESSED = FALSE;
                  switch(grillCursor){
                     case 0:
                        grillCursor = 2;
                        break;
                     case 1:
                        grillCursor = 3;
                        break;
                     default:
                        break;
                  }
               }
               if(B_PRESSED){
                  B_PRESSED = FALSE;
                  actionCursor++;
                  if(actionCursor >= NUMB_ACTIONS_GRILL){
                     actionCursor = 0;
                  }
                  displayActions(actionCursor, currentLocation);
                  delay(20);
               }
               if(A_PRESSED){
                  A_PRESSED = FALSE;
                  switch(actionCursor){
                     case PLACE_PATTY:
                        placePatty(grillCursor);
                        break;
                     case FLIP_PATTY:
                        flipPatty(grillCursor);
                        break;
                     case READY_PATTY:
                        readyPatty(grillCursor);
                        break;
                     case TRASH_PATTY:
                        trashPatty(grillCursor);
                        break;
                  }  
               }
               break;
            case ASSEMBLY:

               if(LEFT_PRESSED){
                  LEFT_PRESSED = FALSE;
                  if(handX > 0){
                     handX--;
                  }
                  delay(20);
               }
               if(RIGHT_PRESSED){
                  RIGHT_PRESSED = FALSE;
                  if(handX < 100){
                     handX++;
                  }
                  delay(20);
               } 
               if(UP_PRESSED){
                  UP_PRESSED = FALSE;
                  // if(actionCursor == PICK_ITEM){
                  //    foodItemCursor++;
                  //    if (foodItemCursor >= NUMB_FOOD_ITEMS){
                  //       foodItemCursor = 0;
                  //    }
                  //    if(foodItemCursor == BURGER_R || foodItemCursor == BURGER_M || foodItemCursor == BURGER_D || foodItemCursor == BURGER_B){
                  //       foodItemCursor = 6;
                  //    }
                  // }
                  // else if (actionCursor == PICK_PATTY){
                  //    pattyStogareIndex++;
                  //    if(pattyStogareIndex >=pattyStorageCount){
                  //       pattyStogareIndex = 0;
                  //    }
                  // }
                  // displayGrabable();
                  pew();
                  delay(10);

               } 
               if(DOWN_PRESSED){
                  DOWN_PRESSED = FALSE;
                  // if(actionCursor == PICK_ITEM){
                  //    if (foodItemCursor == 0){
                  //       foodItemCursor = NUMB_FOOD_ITEMS -1;
                  //    }
                  //    else{
                  //       foodItemCursor--;
                  //    }
                  //    if(foodItemCursor == BURGER_R || foodItemCursor == BURGER_M || foodItemCursor == BURGER_D || foodItemCursor == BURGER_B){
                  //       foodItemCursor = 1;
                  //    }
                  // }
                  // else if (actionCursor == PICK_PATTY){
                  //    if(pattyStogareIndex == 0){
                  //       pattyStogareIndex = pattyStorageCount-1;
                  //    }
                  //    else{
                  //       pattyStogareIndex--;
                  //    }
                  // }
                  // displayGrabable();
                  pew();
                  delay(10);
               }
               if(B_PRESSED){
                  B_PRESSED = FALSE;
                  actionCursor++;
                  if(actionCursor >= NUMB_ACTIONS_ASSEMBLY){
                     actionCursor = 0;
                  }
                  // displayGrabable();
                  displayActions(actionCursor, currentLocation);
                  delay(20);
               }
               if(A_PRESSED){
                  A_PRESSED = FALSE;
                  handOpen = handOpen ^ 1;
                  delay(100);
               }
               //printf("assembly\n");

               displayHand(0);
               break; // end of assemly
            default:
               //printf("something Wrong with game contact developer");
               while(1){}
         }
      }
   }
}
void runEveryLoop(){
     //cls();
  //gotoxy(x, y);

   //DISPLAY_OFF;
   //DISPLAY_ON;
   updateInput();
   updaterTimer(250);//1000);//one second of time in millis
   /*if(SELECT_PRESSED && START_PRESSED){
      printf("start+slect pressed\n");
      SELECT_PRESSED = FALSE;
      START_PRESSED = FALSE;
      paused = TRUE;
   }
   else */
   if(START_PRESSED){
      START_PRESSED = FALSE;
      selectedOrder++;
      if(selectedOrder >= pendingOrderSize){
         selectedOrder = 0;
      }
      displayTicket(&(pendingOrders[selectedOrder]), currentLocation);
   }
   
   if(SELECT_PRESSED){
      SELECT_PRESSED = FALSE;
      if(currentLocation >= STORE){
         currentLocation++;
      }
      if(currentLocation > ASSEMBLY){
         currentLocation = STORE;
      }
      ClearAllTiles();
      HIDE_BKG;
      switch(currentLocation){
         case STORE:
            set_bkg_data(0, 104 , storeTiles);
            //VBK_REG = 1;
            set_bkg_tiles(0,0,20,18,storeMap);
            //VBK_REG = 0;
            break;
         case GRILL:
            set_bkg_data(0, NUMB_GRILL_ASSEMBLY_TILES , grillAssemblyTiles);
            //VBK_REG = 1;
            set_bkg_tiles(0,0,20,36,grillMap);
            //VBK_REG = 0; 
            break;
         case ASSEMBLY:
            set_bkg_data(0, NUMB_GRILL_ASSEMBLY_TILES , grillAssemblyTiles);
            //VBK_REG = 1;
            set_bkg_tiles(0,0,20,36, assemblyMap);
            //VBK_REG = 0; 
            //displayGrabable();
            break;
   
      }

      displayTicket(&(pendingOrders[selectedOrder]), currentLocation);
      // if(currentLocation == GRILL){
      //    set_bkg_tiles(17,3,2,1,topBun);
      //    set_bkg_tiles(17,4,2,1,botBun);
      //    set_bkg_tiles(17,5,2,1,burgerRaw);
      //    set_bkg_tiles(17,6,2,1,burgerMid);
      //    set_bkg_tiles(17,7,2,1,burgerDone);
      //    set_bkg_tiles(17,8,2,1,burgerBurnt);
      //    set_bkg_tiles(17,9,2,1,tomato);
      //    set_bkg_tiles(17,10,2,1,cheese);
      //    set_bkg_tiles(17,11,2,1,lettuce);
      // }
      displayActions(actionCursor, currentLocation);

      SHOW_BKG;
   }

   if(refresh){ //execute every one second
      //cls();
      //gotoxy(0, 0);
      //font_init();
      //printf("%d,%d\n",x,y);

      for(index = 0; index < NUMB_PATTIES;index++){//update the patties percentage
         if(burgers[index]){
            if(burgers_bot_cook[index] < 150){
               burgers_bot_cook[index]++;
            }
            else if(burgers_bot_cook[index] > 150){
               burgers_bot_cook[index] = 150;
            }
         }
      }
      //resetPressedKeys();
      // if(hl2){//toggle boolean, it will change highlight action sprite
      //    hl2 = FALSE;
      // }
      // else{
      //    hl2 = TRUE;
      // }


      timeMinutes++;
      if(timeMinutes >= 60){
         timeMinutes = 0;
         timeHours++;
      }
      if(timeHours > 12){
         timeHours = 1;
         timeMinutes = 0;
      }
      if(currentLocation == GRILL || currentLocation == ASSEMBLY){
         setTime(timeHours,timeMinutes);
      }
      resetTimerFlag();
      
         
   }
}


void loadSaved(){

}


UINT8 loadExists(){
   return FALSE;

}
void updateInput(){
   if(joypad() & J_START && joypad() & J_SELECT){  // If DOWN is pressed
      paused = TRUE;
      //printf("start pressed\n");
   }
   if(joypad()==J_RIGHT){ // If RIGHT is pressed
      RIGHT_PRESSED = TRUE;
   } 
   if(joypad()==J_LEFT){  // If LEFT is pressed
      LEFT_PRESSED = TRUE;
   }
      
   if(joypad()==J_UP){  // If UP is pressed 
      UP_PRESSED = TRUE;
   }
   
   if(joypad()==J_DOWN){  // If DOWN is pressed
      DOWN_PRESSED = TRUE;
   }
   if(joypad()==J_A){  // If DOWN is pressed
      A_PRESSED = TRUE;
      delay(100);
   }      
   if(joypad()==J_B){ // If DOWN is pressed
      B_PRESSED = TRUE;
      delay(100);
   }
   if(joypad()==J_SELECT){  // If DOWN is pressed
      SELECT_PRESSED = TRUE;
      //printf("slect pressed\n");
      delay(100);
   }
   if(joypad()==J_START){  // If DOWN is pressed
      START_PRESSED = TRUE;
      //printf("start pressed\n");
      delay(100);
   }

   //waitpadup();
}

void updaterTimer(time_t inTicks){//input is how many millis to trigger
   currentTime = clock();
   timeSum += (currentTime - lastTime);
   lastTime = currentTime;
   if(timeSum*10 >= inTicks){
      refresh = TRUE;
   }
}
void resetTimerFlag(){
   refresh = FALSE;
   timeSum = 0;
}


UINT8 getNewOID(){
   return nextOrderID++;
}

// void displayNumber(UINT16 numb, UINT8 X, UINT8 Y, UINT8 sprite0, UINT8 sprite1, UINT8 sprite2){
//    if(numb > 999){
//       numb = 999;
//    }
//    //printf("%d\n",numb );
//    set_sprite_tile(sprite0, numb/100);
//    move_sprite(sprite0,X,Y);
//    set_sprite_tile(sprite1, (numb%100)/10);
//    move_sprite(sprite1,X+OFFSET,Y);
//    set_sprite_tile(sprite2, (numb%100)%10 );
//    move_sprite(sprite2,X+OFFSET+OFFSET,Y);
// }

void resetPressedKeys(){
   A_PRESSED      = FALSE;
   B_PRESSED      = FALSE;
   UP_PRESSED     = FALSE;
   DOWN_PRESSED   = FALSE;
   LEFT_PRESSED   = FALSE;
   RIGHT_PRESSED  = FALSE;
   SELECT_PRESSED = FALSE;
   START_PRESSED  = FALSE;
}


void placePatty(UINT8 pattyNumb){
   if(burgers[pattyNumb] == FALSE){
      //printf("placing burger at %d\n",pattyNumb);
      pew();
      burgers[pattyNumb] = TRUE;
      burgers_bot_cook[pattyNumb] = 0;
      burgers_top_cook[pattyNumb] = 0;
   }
}
void flipPatty(UINT8 pattyNumb){
   //printf("flipping %d\n",pattyNumb);
   pew();
   temp = burgers_bot_cook[pattyNumb];
   burgers_bot_cook[pattyNumb] = burgers_top_cook[pattyNumb];
   burgers_top_cook[pattyNumb] = temp;
}
void readyPatty(UINT8 pattyNumb){
   if(pattyStorageCount >= MAX_PATTY_STORAGE){
      //printf("cooked storage full,max: %d\n",MAX_PATTY_STORAGE);
   }
   else if(burgers[pattyNumb]){//top bottom configuration
      //printf("storing patty %d\n",pattyNumb);
      pattyStorage[pattyStorageCount*2]     = burgers_top_cook[pattyNumb];
      pattyStorage[(pattyStorageCount*2)+1] = burgers_bot_cook[pattyNumb];
      pattyStorageCount++;
      pew();
      burgers[pattyNumb] = FALSE;
      burgers_bot_cook[pattyNumb] = 0;
      burgers_top_cook[pattyNumb] = 0;
   }
   else{
      //printf("no patty in grill to store\n");
   }
}
void trashPatty(UINT8 pattyNumb){
   //printf("trashed %d\n",pattyNumb);
   pew();
   burgers[pattyNumb] = FALSE;
   burgers_bot_cook[pattyNumb] = 0;
   burgers_top_cook[pattyNumb] = 0;
}

void ClearAllTiles(){
   for(temp = 0; temp <= 100;temp++){
      set_sprite_tile(temp, BLANK_TILE);
   }
}


void pew(){
   NR52_REG = 0x80;
   NR51_REG = 0x11;
   NR50_REG = 0x77;

   NR10_REG = 0x1E;
   NR11_REG = 0x10;
   NR12_REG = 0xF3;
   NR13_REG = 0x00;
   NR14_REG = 0x87;
}

 

void setOderNumber(UINT8 numb){
   if(numb > 99){
      numb == 99;
   }
   placeDigit(numb/10,17,0);
   placeDigit(numb%10,18,0);

}
void placeDigit(UINT8 digit,UINT8 x, UINT8 y){
   switch(digit){
      case 0:
         set_bkg_tiles(x,y,1,2,digit0);
         break;
      case 1:
         set_bkg_tiles(x,y,1,2,digit1);
         break;
      case 2:
         set_bkg_tiles(x,y,1,2,digit2);
         break;
      case 3:
         set_bkg_tiles(x,y,1,2,digit3);
         break;
      case 4:
         set_bkg_tiles(x,y,1,2,digit4);
         break;
      case 5:
         set_bkg_tiles(x,y,1,2,digit5);
         break;
      case 6:
         set_bkg_tiles(x,y,1,2,digit6);
         break;
      case 7:
         set_bkg_tiles(x,y,1,2,digit7);
         break;
      case 8:
         set_bkg_tiles(x,y,1,2,digit8);
         break;
      case 9:
         set_bkg_tiles(x,y,1,2,digit9);
         break;

   }
}



void setTime(UINT8 hours,UINT8 minutes){
   placeDigit(hours/10,2,0);
   placeDigit(hours%10,3,0);
   placeDigit(minutes/10,5,0);
   placeDigit(minutes%10,6,0);
}


void displayPatty(UINT8 patty,UINT8 top,UINT8 bot){
   x = 0;
   y = 0;
   switch(patty){
      case 0:
         x = 5;
         y = 7;
         break;
      case 1:
         x = 8;
         y = 7;
         break;
      case 2:
         x = 5;
         y = 10;
         break;
      case 3:
         x = 8;
         y = 10;
         break;
   }
   switch(top){
      case 0:
         set_bkg_tiles(x,y,3,1,pt0);
         break;
      case 1:
         set_bkg_tiles(x,y,3,1,pt1);
         break;
      case 2:
         set_bkg_tiles(x,y,3,1,pt2);
         break;
      case 3:
         set_bkg_tiles(x,y,3,1,pt3);
         break;
      default:
         set_bkg_tiles(x,y,3,1,p4);
         break;
   }
   switch(bot){
      case 0:
         set_bkg_tiles(x,y+1,3,1,pb0);
         break;
      case 1:
         set_bkg_tiles(x,y+1,3,1,pb1);
         break;
      case 2:
         set_bkg_tiles(x,y+1,3,1,pb2);
         break;
      case 3:
         set_bkg_tiles(x,y+1,3,1,pb3);
         break;
      default:
         set_bkg_tiles(x,y+1,3,1,p4);
         break;
   }

}
#define NUMB_ACTIONS_ASSEMBLY 3
#define PICK_ITEM 0
#define PICK_PATTY  1
#define READY_TICKET 2
#define TRASH_PATTY 3


void displayActions(UINT8 action,UINT8 screen){
   if(screen == GRILL){
      switch(action){
         case PLACE_PATTY:
            set_bkg_tiles(17,15,2,2,placeMap);
            break;
         case FLIP_PATTY:
            set_bkg_tiles(17,15,2,2,flipMap);
            break;
         case READY_PATTY:
            set_bkg_tiles(17,15,2,2,readyMap);
            break;
         case TRASH_PATTY:
            set_bkg_tiles(17,15,2,2,trashMap);
            break;
      }
   }
   else if(screen == ASSEMBLY){
      switch(action){
         case PICK_ITEM:
            set_bkg_tiles(17,15,2,2,handMap);
            break;
         case PICK_PATTY:
            set_bkg_tiles(17,15,2,2,placeMap);
            break;
         case READY_PATTY:
            set_bkg_tiles(17,15,2,2,readyMap);
            break;
         case TRASH_PATTY:
            set_bkg_tiles(17,15,2,2,trashMap);
            break;
      }
   }
}


void displayHand(UINT8 spriteNumb){
      set_sprite_tile(spriteNumb,ARM_TILE);
      set_sprite_tile(spriteNumb+1,ARM_TILE+1);
      set_sprite_tile(spriteNumb+2,ARM_TILE+2);
      set_sprite_tile(spriteNumb+3,ARM_TILE+3);
   if(handOpen == FALSE){
      set_sprite_tile(spriteNumb+4,CLOSED_HAND_TILE);
      set_sprite_tile(spriteNumb+5,CLOSED_HAND_TILE+1);
      set_sprite_tile(spriteNumb+6,CLOSED_HAND_TILE+2);
      set_sprite_tile(spriteNumb+7,CLOSED_HAND_TILE+3);
   }
   else{
      set_sprite_tile(spriteNumb+4,FIST_TILE);
      set_sprite_tile(spriteNumb+5,FIST_TILE+1);
      set_sprite_tile(spriteNumb+6,FIST_TILE+2);
      set_sprite_tile(spriteNumb+7,FIST_TILE+3);
   }
   moveHand(0,handX,handY);
}


void moveHand(UINT8 spriteNumb, UINT8 x,UINT8 y){
   move_sprite(spriteNumb,   x, y);
   move_sprite(spriteNumb+2, x+OFFSET, y);
   move_sprite(spriteNumb+1, x, y+OFFSET);
   move_sprite(spriteNumb+3, x+OFFSET, y+OFFSET);
   move_sprite(spriteNumb+4, x+(2*OFFSET), y);
   move_sprite(spriteNumb+6, x+(3*OFFSET), y);
   move_sprite(spriteNumb+5, x+(2*OFFSET), y+OFFSET);
   move_sprite(spriteNumb+7, x+(3*OFFSET), y+OFFSET);
}




void randomTicketGenerator(struct Ticket* tik){ //pass in the address of exixting ticket

   tik->orderID = getNewOID();
   tik->ingredients_size = 0;
   tik->number_patties = 0;
   tik->ingredients[tik->ingredients_size++] = BOT_BUN;

   if((rand()&1) == 1){
      tik->ingredients[tik->ingredients_size] = BURGER_M;
      tik->ingredients_size++;
      tik->number_patties++;
   }
   else{
      tik->ingredients[tik->ingredients_size] = BURGER_D;
      tik->ingredients_size++;
      tik->number_patties++;
   }

   if((rand()&1) == 1){
      tik->ingredients[tik->ingredients_size] = CHEESE;
      tik->ingredients_size++;
   }
   if((rand()&1) == 1){
      if((rand()&1) == 1){
         if((rand()&1) == 1){
            tik->ingredients[tik->ingredients_size] = BURGER_M;
            tik->ingredients_size++;
            tik->number_patties++;
         }
         else{
            tik->ingredients[tik->ingredients_size] = BURGER_D;
            tik->ingredients_size++;
            tik->number_patties++;
         }
      }
      if((rand()&1) == 1){
         tik->ingredients[tik->ingredients_size] = CHEESE;
         tik->ingredients_size++;
      }
   }
   if((rand()&1) == 1){
      tik->ingredients[tik->ingredients_size] = TOMATO;
      tik->ingredients_size++;
   }
   if((rand()&1) == 1){
      tik->ingredients[tik->ingredients_size] = LETUCE;
      tik->ingredients_size++;
   }
   tik->ingredients[tik->ingredients_size++] = TOP_BUN;

} 

// #define TOP_BUN 0
// #define BOT_BUN 1
// #define BURGER_R  2
// #define BURGER_M  3
// #define BURGER_D  4
// #define BURGER_B  5
// #define TOMATO  6
// #define CHEESE  7
// #define LETUCE  8

//    UINT8 ingredients[20];//ingredient 0 should be the bottom bun, len-1 should be top bun
//    UINT8 ingredients_size;
//    UINT8 patty_tops[3];
//    UINT8 patty_bots[3];
//    UINT8 number_patties;

void displayTicket(struct Ticket* tik, UINT8 location){//pass int addres of object using the & operator
   if (location == GRILL || location == ASSEMBLY){
      UINT8 x = 17;
      UINT8 y = 3 + 10 -1;
      UINT8 tempIndex = 0;
      setOderNumber(tik->orderID);
      while (tempIndex< 10 ){
               set_bkg_tiles(x,y,2,1,blankItem);
         tempIndex++;
         y--;
      }
      x = 17;
      y = 3 + tik->ingredients_size -1;
      tempIndex = 0;
      while (tempIndex< tik->ingredients_size ){
         switch(tik->ingredients[tempIndex]){
            case TOP_BUN:
               set_bkg_tiles(x,y,2,1,topBun);
               break;
            case BOT_BUN:
               set_bkg_tiles(x,y,2,1,botBun);
               break;
            case BURGER_R:
               set_bkg_tiles(x,y,2,1,burgerRaw);
               break;
            case BURGER_M:
               set_bkg_tiles(x,y,2,1,burgerMid);
               break;
            case BURGER_D:
               set_bkg_tiles(x,y,2,1,burgerDone);
               break;
            case BURGER_B:
               set_bkg_tiles(x,y,2,1,burgerBurnt);
               break;
            case TOMATO:
               set_bkg_tiles(x,y,2,1,tomato);
               break;
            case CHEESE:
               set_bkg_tiles(x,y,2,1,cheese);
               break;
            case LETUCE:
               set_bkg_tiles(x,y,2,1,lettuce);
               break;
         }
         tempIndex++;
         y--;
      }
   }
}


void newPendingOrder(){
   if (pendingOrderSize <= MAX_PENDING_ORDERS){
      randomTicketGenerator(&(pendingOrders[pendingOrderSize++])); //pass in the address of exixting ticket
   }
}



 UINT8 burgerClass(UINT8 cookness){//0:Raw,1:Medium,2:Done,3:Burnt
   if(cookness < 70){
      return 0;
   }
   else if(cookness >= 70 && cookness < 90){
      return 1;
   }
   else if(cookness >= 91 && cookness < 110){
      return 2;
      burgers_top_cook[index] = 91;
   }
   else if(cookness >= 111){
      return 3;
   }
}



void displayFoodItem(UINT8 itemID){
   UINT8 x = 13;
   UINT8 y = 3 ;
   switch(itemID){
      case TOP_BUN:
         set_bkg_tiles(x,y,2,1,topBun);
         break;
      case BOT_BUN:
         set_bkg_tiles(x,y,2,1,botBun);
         break;
      case BURGER_R:
         set_bkg_tiles(x,y,2,1,burgerRaw);
         break;
      case BURGER_M:
         set_bkg_tiles(x,y,2,1,burgerMid);
         break;
      case BURGER_D:
         set_bkg_tiles(x,y,2,1,burgerDone);
         break;
      case BURGER_B:
         set_bkg_tiles(x,y,2,1,burgerBurnt);
         break;
      case TOMATO:
         set_bkg_tiles(x,y,2,1,tomato);
         break;
      case CHEESE:
         set_bkg_tiles(x,y,2,1,cheese);
         break;
      case LETUCE:
         set_bkg_tiles(x,y,2,1,lettuce);
         break;
      default:
         set_bkg_tiles(x,y,2,1,blankItem);
         break;
   }
   delay(100);
}
// void compareOrder(){
// // diplay blank order, add to the logic in the displa, if sixe is 0 then display blank
// }
//UINT8 burgerGrade(UINT8 classToCompare, UINT8 top, bot)//will return a score bor the burger
//UINT8 ticketGrade(Ticket request, Ticket delivered)


// UINT8 averagBurgerAndClass(UINT8 ind){//index from storage

// }

// void displayGrabable(){
//    delay(2000);
//    pew();
//    pew();
//    if(actionCursor == PICK_PATTY){
//       if (pattyStorageCount <= 0){
//       //   displayFoodItem(NUMB_FOOD_ITEMS);//display empty
//       }
//       else{
//          pew();
//          //delay(2000);
//          displayFoodItem(BURGER_R);//+burgerClass((pattyStorage[pattyStogareIndex*2] +pattyStorage[(pattyStogareIndex*2)+1])/2));//display as the average of the two parts
//       }
//    }
//    else{
//       displayFoodItem(foodItemCursor);
//    }
// }














